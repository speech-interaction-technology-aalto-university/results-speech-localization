# Results-speech-localization
This repository provides results/plots of the paper "Speech Localization at Low Bitrates in Wireless Acoustics Sensor Networks" published by Frontiers in Signal Processing, section Signal Processing Theory" doi: 10.3389/frsip.2022.800003


## Motivation
The purpose of this repository is to reproduce Source localization results which are stored in standardized json files that can be easiliy reproduced and used. Furthermore we want to encourage users to use this evaluation output format as the standardized way to share source localization results for processed tracks. In conjunction with our source localization results, we provide you with a Jupyter notebook to read the json file and plot its corresponding results.
    

## How to use?

A Jupyter notebook .ipynb is provided to read the .Json file and plot the corresponding figure.

## Dataset

The evaluation of the localization model is performed on simulated datasets on the public available Librispeech corpus.

##  Requirements

Python 3.x

matplotlib

numpy 

seaborn 

pandas 

altair 

## Reference
https://gitlab.com/speech-interaction-technology-aalto-university/results-speech-localization/

https://www.frontiersin.org/articles/10.3389/frsip.2022.800003/abstract